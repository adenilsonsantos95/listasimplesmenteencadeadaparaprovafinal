package Teste;

import Codigo.ListaSimplismenteEncadeada;

public class Teste {

	public static void main(String[] args) {
		ListaSimplismenteEncadeada<Integer> lista = new ListaSimplismenteEncadeada<>();
		lista.incluir(1);
		lista.incluir(2);
		lista.incluir(3);
		lista.incluir(4);
		lista.incluir(5);
		lista.incluir(6);
		System.out.println(lista);
		lista.inverter();
		System.out.println(lista);
		
	}

}
