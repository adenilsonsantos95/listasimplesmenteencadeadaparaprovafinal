package Codigo;

public class ListaSimplismenteEncadeada<T> implements TDAListaSimplesmenteEncadeada<T> {
	private No primeiro;
	private No ultimo;
	private int quantidade;
	
	public ListaSimplismenteEncadeada() {
		this.primeiro = null;
		this.ultimo = null;
	}

	@Override
	public void incluir(T elemento) {
		if(this.estaVazia()) {
			this.incluirInicio(elemento);
		}else {
			No no = new No(elemento);
			this.ultimo.setProximo(no);
			this.ultimo = no;
			this.quantidade++;
		}
	}
	
	
	
	
	
	public void inverter() {
		this.quantidade--;
		int aux = (int) this.primeiro.getElemento();
		this.primeiro.setElemento(this.ultimo.getElemento());
		this.ultimo.setElemento(aux);
		if(this.primeiro.getElemento() == this.ultimo.getElemento()) {
			inverter();
		}
		}

	@Override
	public void incluirInicio(T elemento) {
		No no = new No(elemento);
		if(this.estaVazia()) {
			this.primeiro = no;
			this.ultimo = this.primeiro;
			//this.primeiro.setProximo(this.ultimo);
		}else {
			no.setProximo(this.primeiro);
			this.primeiro = no;
		}
		this.quantidade++;
	}

	@Override
	public void incluir(T elemento, int posicao) {
		if(this.posicaoInvalida(posicao) || elemento == null) {
			System.out.println("A posicao e invalida ou elemento e nulo.");
		}else if(posicao == 0 || this.estaVazia()){
			this.incluirInicio(elemento);
		}else if(posicao == this.tamanho()){
			No no = new No(elemento);
			this.ultimo.setProximo(no);
			this.ultimo = no;
			this.quantidade++;
		}else {
			No no  = new No(elemento);
			No aux = this.primeiro;
			No temp = aux;
			for (int i = 0; i < this.tamanho(); i++) {
				if(i == posicao) {
					no.setProximo(aux);
					temp.setProximo(no);
					break;
				}
				temp = aux;
				aux = aux.getProximo();
			}
			this.quantidade++;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public T obterDaPosicao(int posicao) {
		if(this.estaVazia()) {
			return null;
		}else if(this.posicaoInvalida(posicao)) {
			return null;
		}else if(posicao == 0){
			return (T) this.primeiro.getElemento();
		}else if(posicao == this.tamanho()){
			return (T) this.ultimo.getElemento();
		}else {
			No aux = this.primeiro;
			for (int i = 0; i < posicao; i++) {
				aux = aux.getProximo();
			}
			return (T) aux.getElemento();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public T obter(T item) {
		if(this.estaVazia()) {
			return null;
		}else if(this.primeiro.getElemento().equals(item)) {
			return (T) this.primeiro.getElemento();
		}else if(this.ultimo.getElemento().equals(item)) {
			return (T) this.ultimo.getElemento();
		}else {
			No aux = this.primeiro;
			for (int i = 0; i < this.tamanho(); i++) {
				if(aux.getElemento().equals(item)) {
					return (T) aux.getElemento();
				}
				aux = aux.getProximo();
			}
		}
		
		return null;
	}

	@Override
	public void remover(int posicao) {
		if(this.estaVazia() || this.posicaoInvalida(posicao)) {
			System.out.println("A lista esta vazia ou a posicao e invalida.");
		}else if(posicao == 0) {
			this.removerOPrimeiro();
		}else if(posicao == this.tamanho()) {
			this.removerOUltimo();
		}else{
			No aux = this.primeiro;
			No temp = aux;
			for (int i = 0; i < this.tamanho(); i++) {
				if(i == posicao) {
					temp.setProximo(aux.getProximo());
					break;
				}
				temp = aux;
				aux = aux.getProximo();
			}
			this.quantidade--;
		}
	}

	@Override
	public void remover(T elemento) {
		if(this.estaVazia() || !this.contem(elemento)) {
			System.out.println("A lista esta vazia ou elemento nao existe na lista.");
		}else if(this.primeiro.getElemento().equals(elemento)){
			this.removerOPrimeiro();
		}else if(this.ultimo.getElemento().equals(elemento)){
			this.removerOUltimo();
		}else {
			No aux = this.primeiro;
			No temp = null;
			for (int i = 0; i < this.tamanho(); i++) {
				if(aux.getElemento().equals(elemento)) {
					temp.setProximo(aux.getProximo());
					break;
				}
				temp = aux;
				aux = aux.getProximo();
			}
			this.quantidade--;
		}
	}
	
	@SuppressWarnings("unused")
	private void removerOPrimeiro() {
		this.primeiro = this.primeiro.getProximo();
		this.quantidade--;
	}
	
	@SuppressWarnings("unused")
	private void removerOUltimo() {
		No aux = this.primeiro;
		for (int i = 0; aux.getProximo() != this.ultimo; i++) {
			aux = aux.getProximo();
		}
		this.ultimo = aux;
		this.ultimo.setProximo(null);
		this.quantidade--;
	}

	@Override
	public void limpar() {
		this.primeiro = null;
		this.ultimo = null;
	}

	@Override
	public void aumentarCapacidade() {
		
	}

	@Override
	public int tamanho() {
		return this.quantidade;
	}

	@Override
	public boolean contem(T item) {
		if(this.estaVazia()) {
			return false;
		}else if(this.primeiro.getElemento().equals(item)){
			return true;
		}else if(this.ultimo.getElemento().equals(item)){
			return true;
		}else {
			No aux = this.primeiro;
			for (int i = 0; i < this.tamanho(); i++) {
				if(aux.getElemento().equals(item)) {
					return true;
				}
				aux = aux.getProximo();
			}
		}
		return false;
	}

	@Override
	public boolean verificarInicializacao() {
		return false;
	}
	
	protected boolean posicaoInvalida(int posicao) {
		return posicao < 0 || posicao > this.tamanho();
	}

	@Override
	public boolean estaVazia() {
		return this.quantidade == 0;
	}

	@Override
	public boolean estaCheia() {
		return false;
	}

	@Override
	public String toString() {
		if(this.estaVazia()) {
			return "ListaSimplismenteEncadeada []";
		}
		String print =  "ListaSimplismenteEncadeada [ ";
		No aux;
		for (aux = this.primeiro; aux != this.ultimo; aux = aux.getProximo()) {
			print = print + aux.getElemento() + ", ";
		}
		return print + aux.getElemento() + "] - tamanho = " + this.tamanho();
	}
	
	
}
